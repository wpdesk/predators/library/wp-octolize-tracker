<?php

namespace OptInNotice;

use Octolize\Tracker\OptInNotice\OptInNotice;
use Octolize\Tracker\OptInNotice\ShouldDisplayAlways;
use Octolize\Tracker\OptInNotice\ShouldDisplayNever;
use WPDesk\PluginBuilder\Plugin\Hookable;

class OptInNoticeTest extends \WP_Mock\Tools\TestCase {
	const HTTPS_OCTOLIZE_COM = 'https://octolize.com';

	/**
	 * @var OptInNotice
	 */
	private $opt_in_notice_under_test;

	public function setUp() {
		\WP_Mock::setUp();

		$this->opt_in_notice_under_test = $this->getMockBuilder( OptInNotice::class )
		                                    ->setMethods( [ 'create_notice' ] )
											->setConstructorArgs( [ 'slug', self::HTTPS_OCTOLIZE_COM, new ShouldDisplayAlways() ] )
											->getMock();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function testShouldImplementsHookable() {
		// Then
		$this->assertInstanceOf( Hookable::class, $this->opt_in_notice_under_test );
	}

	public function testShouldAddHooks() {
		// Expects
		\WP_Mock::expectActionAdded( 'admin_notices', [ $this->opt_in_notice_under_test, 'display_notice_if_should' ] );

		// When
		$this->opt_in_notice_under_test->hooks();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldDisplayNoticeIfShould() {
		// Expects
		$this->opt_in_notice_under_test->expects( $this->once() )->method( 'create_notice' )->withAnyParameters();

		// When
		$this->opt_in_notice_under_test->display_notice_if_should();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldNotDisplayNoticeIfNotShould() {
		// Given
		$this->opt_in_notice_under_test = $this->getMockBuilder( OptInNotice::class )
		                                    ->setMethods( [ 'create_notice' ] )
		                                    ->setConstructorArgs( [ 'slug', self::HTTPS_OCTOLIZE_COM, new ShouldDisplayNever() ] )
		                                    ->getMock();

		// Expects
		$this->opt_in_notice_under_test->expects( $this->never() )->method( 'create_notice' )->withAnyParameters();

		// When
		$this->opt_in_notice_under_test->display_notice_if_should();

		// Then
		$this->assertTrue( true );
	}

}
