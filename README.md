[![pipeline status](https://gitlab.com/wpdesk/predators/library/wp-octolize-tracker/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/predators/library/wp-octolize-tracker/pipelines)
[![coverage report](https://gitlab.com/wpdesk/predators/library/wp-octolize-tracker/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/predators/library/wp-octolize-tracker/commits/master)
[![Latest Stable Version](https://poser.pugx.org/octolize/wp-octolize-tracker/v/stable)](https://packagist.org/packages/octolize/wp-octolize-tracker)
[![Total Downloads](https://poser.pugx.org/octolize/wp-octolize-tracker/downloads)](https://packagist.org/packages/octolize/wp-octolize-tracker)
[![License](https://poser.pugx.org/octolize/wp-octolize-tracker/license)](https://packagist.org/packages/octolize/wp-octolize-tracker)

Octolize Tracker
================

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via Composer run the following command:

```bash
composer require octolize/wp-octolize-tracker
```

## Example usage

### Initialize tracker in Octolize Plugin

```php
$shops = $this->plugin_info->get_plugin_shops();
$shop_url = $shops[get_locale()] ?? ( $shops['default'] ?? 'https://octolize.com' );
$this->add_hookable( new TrackerInitializer(
	$this->plugin_info->get_plugin_file_name(),
	$this->plugin_info->get_plugin_slug(),
	$this->plugin_info->get_plugin_name(),
	$shop_url
) );
```

## Author

[Octolize](https://octolize.com)
