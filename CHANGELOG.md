## [1.8.0] - 2024-08-20
### Added
- New reason for deactivation

## [1.7.0] - 2024-08-19
### Changed
- Removed filter `wpdesk_opt_link_location`

## [1.6.1] - 2024-07-15
### Fixed
- text domain

## [1.6.0] - 2024-05-16
### Changed
- Opt in/out link location by filter `wpdesk_opt_link_location` 

## [1.5.2] - 2024-03-08
### Changed
- Disabled notices registered by default by `wp-wpdesk-tracker`
- Removed own instantiation of `wp-wpdesk-tracker` UI extensions to avoid duplicates after `wp-plugin-flow-common` update.

## [1.5.1] - 2024-03-08
### Fixed
- PRO plugins deactivation reasons constructor

## [1.5.0] - 2024-03-08
### Added
- PRO plugins deactivation reasons

## [1.4.1] - 2024-03-07
### Fixed
- nonce verification in AJAX actions

## [1.4.0] - 2024-03-01
### Added
- reasons factory for from shipping method factory

## [1.3.2] - 2024-02-28
### Changed
- PL translation

## [1.3.1] - 2024-02-27
### Fixed
- Default reason

## [1.3.0] - 2024-02-26
### Changed
- Deactivation tracker layout

## [1.2.1] - 2022-10-19
### Fixed
- Multiple tracker instances

## [1.2.0] - 2022-10-17
### Added
- Opt In notice

## [1.1.0] - 2022-06-20
### Added
- Complete tracker initializer - creates tracker and deactivation tracker.

## [1.0.2] - 2022-06-20
### Changed
- Sender Creator hook

## [1.0.0] - 2022-06-14
### Added
- Initial version.
